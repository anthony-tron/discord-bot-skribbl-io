# discord-bot-skribbl-io

A discord bot that can create skribbl.io rooms that can handle custom words from Pokemon, Mario, Zelda, and more.

# Compile and run

Compile with `tsc`
Run with `./run.sh`

# Discord token

You will need to specify the token of your bot in .token.
Just copy and paste the token in it. Don't put anything else and don't add this file in a public git.
