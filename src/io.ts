import * as io from 'socket.io-client';

const params = {
    "avatar": [
        9,
        10,
        23,
        -1
    ],
    "code": "",
    "createPrivate": true,
    "join": "",
    "language": "English",
    "name": "Welcome Node.JS"
};

const p = io('https://skribbl.io:4999')
p.emit('login', params);

p.on('connect', () => {
    console.log('Socket.IO is connected');
});

p.on('disconnect', () => {
    console.log('Socket.IO has disconnected');
});

p.on('result', (value: any) => {
    console.log(value.host);

    const m = io(value.host);

    m.on('connect', () => {
        console.log('Socket.IO 2 is connected');
        m.emit('userData', params);
        m.emit('lobbySetCustomWordsExclusive', true);

        setTimeout(() => {
            m.emit('lobbyGameStart', 'booba, kaaris, jul, alkpote');
            m.close();
        }, 10000);
    });

    m.on('disconnect', () => {
        console.log('Socket.IO 2 has disconnected');
    });

    m.on('lobbyConnected', (value: any) => {
        console.log('Join with: https://skribbl.io/?' + value.key);
    });

    p.close();
});

console.log('Hello, world!');