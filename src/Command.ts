export class Command {
    public name: String;
    public args: {};

    private constructor(name: String, args: {}) {
        this.name = name;
        this.args = args;
    }

    /**
     * parse
     */
    public static parse(rawString: String): Command {
        let trimmed = rawString.trim();
        if (trimmed.length === 0)
            throw Error();
        
        if(!trimmed.includes(' '))
            return new Command(trimmed, {});

        let args = {}
        const commandName: String = trimmed.substring(0, trimmed.indexOf(' '));
        
        for(let i = trimmed.indexOf(' '); i < trimmed.length; ++i) {
            if (trimmed[i] == '-') {
                let nextSpaceIndex = trimmed.indexOf(' ', i);
                let argName;

                if (nextSpaceIndex == -1) {
                    argName = trimmed.substring(i);
                    args[argName] = true;
                    break;
                } else {
                    argName = trimmed.substring(i, nextSpaceIndex);
                    
                    i = nextSpaceIndex + 1;

                    let nextFlagIndex = trimmed.indexOf('-', i);
                    let argValue : String;

                    if (nextFlagIndex == -1) {
                        argValue = trimmed.substring(i);
                        args[argName] = argValue;
                        break;
                    } else {
                        argValue = trimmed.substring(i, nextFlagIndex - 1);
                        args[argName] = argValue;
                    }

                    i = nextFlagIndex - 1;

                }
            }
        }

        return new Command(commandName, args);
    }
}