import * as io from 'socket.io-client';

export const usage : String = "!skribbl.io [--pokemon [1] [2] [3] [4]]";
export const exampleUsage : String = "!skribbl.io --pokemon 1 2 3 4";
export const AWAIT : number = 1;

const botParams = {
    "avatar": [
        9,
        10,
        23,
        -1
    ],
    "code": "",
    "createPrivate": true,
    "join": "",
    "language": "English",
    "name": "Animator"
};


export class Game {
    public customWords : String;
    readonly delayBeforeGameStarts : number;
    readonly drawTime : number;
    readonly roundNumber : number;
    public url : any;

    public onStart : Function;
    public onConnect : Function;

    private host : SocketIOClient.Socket;

    constructor({ delayBeforeGameStarts = 10000, drawTime = 90, roundNumber = 5 } = {}) {
        this.delayBeforeGameStarts = delayBeforeGameStarts;
        this.drawTime = drawTime;
        this.roundNumber = roundNumber;
    }

    public useCustomWords(words: String) {
        this.customWords = words;
    }

    public create() {
        const connection = io('https://skribbl.io:4999');

        connection.on('connect', () => {
            console.log('Socket.io connected');
        });
        connection.on('disconnect', () => {
            console.log('Socket.io disconnected');
        });

        connection.on('result', (value : any) => {
            this.host = io(value.host);

            this.host.on('connect', () => {
                console.log('Host connected');

                this.host.emit('userData', botParams);
                this.host.emit('lobbySetCustomWordsExclusive', true);
                this.host.emit('lobbySetDrawTime', this.drawTime);
                this.host.emit('lobbySetRounds', this.roundNumber);

                if (this.delayBeforeGameStarts >= 0) {
                    setTimeout(() => {
                        this.start();
                    }, this.delayBeforeGameStarts);
                }
            });

            this.host.on('disconnect', () => {
                console.log('Host disconnected');
            });

            this.host.on('lobbyConnected', (value: any) => {
                this.url = 'https://skribbl.io/?' + value.key;
                this.onConnect();
            });

            connection.close();
        });

        connection.emit('login', botParams);
    }

    public start() {
        this.host.on('lobbyGameStart', () => this.host.close()); // the game has started
        this.host.emit('lobbyGameStart', this.customWords); // request to start the game
        this.onStart();
    }
}