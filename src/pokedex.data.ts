export const data = [
    {
      "index": 1,
      "name": "Bulbizarre",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 2,
      "name": "Herbizarre",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 3,
      "name": "Florizarre",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 4,
      "name": "Salamèche",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 5,
      "name": "Reptincel",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 6,
      "name": "Dracaufeu",
      "types": [
        "Feu",
        "Vol"
      ]
    },
    {
      "index": 7,
      "name": "Carapuce",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 8,
      "name": "Carabaffe",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 9,
      "name": "Tortank",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 10,
      "name": "Chenipan",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 11,
      "name": "Chrysacier",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 12,
      "name": "Papilusion",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 13,
      "name": "Aspicot",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 14,
      "name": "Coconfort",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 15,
      "name": "Dardargnan",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 16,
      "name": "Roucool",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 17,
      "name": "Roucoups",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 18,
      "name": "Roucarnage",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 19,
      "name": "Rattata",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 20,
      "name": "Rattatac",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 21,
      "name": "Piafabec",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 22,
      "name": "Rapasdepic",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 23,
      "name": "Abo",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 24,
      "name": "Arbok",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 25,
      "name": "Pikachu",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 26,
      "name": "Raichu",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 27,
      "name": "Sabelette",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 28,
      "name": "Sablaireau",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 29,
      "name": "Nidoran♀",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 30,
      "name": "Nidorina",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 31,
      "name": "Nidoqueen",
      "types": [
        "Poison",
        "Sol"
      ]
    },
    {
      "index": 32,
      "name": "Nidoran♂",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 33,
      "name": "Nidorino",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 34,
      "name": "Nidoking",
      "types": [
        "Poison",
        "Sol"
      ]
    },
    {
      "index": 35,
      "name": "Mélofée",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 36,
      "name": "Mélodelfe",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 37,
      "name": "Goupix",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 38,
      "name": "Feunard",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 39,
      "name": "Rondoudou",
      "types": [
        "Normal",
        "Fée"
      ]
    },
    {
      "index": 40,
      "name": "Grodoudou",
      "types": [
        "Normal",
        "Fée"
      ]
    },
    {
      "index": 41,
      "name": "Nosferapti",
      "types": [
        "Poison",
        "Vol"
      ]
    },
    {
      "index": 42,
      "name": "Nosferalto",
      "types": [
        "Poison",
        "Vol"
      ]
    },
    {
      "index": 43,
      "name": "Mystherbe",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 44,
      "name": "Ortide",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 45,
      "name": "Rafflesia",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 46,
      "name": "Paras",
      "types": [
        "Insecte",
        "Plante"
      ]
    },
    {
      "index": 47,
      "name": "Parasect",
      "types": [
        "Insecte",
        "Plante"
      ]
    },
    {
      "index": 48,
      "name": "Mimitoss",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 49,
      "name": "Aéromite",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 50,
      "name": "Taupiqueur",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 51,
      "name": "Triopikeur",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 52,
      "name": "Miaouss",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 53,
      "name": "Persian",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 54,
      "name": "Psykokwak",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 55,
      "name": "Akwakwak",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 56,
      "name": "Férosinge",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 57,
      "name": "Colossinge",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 58,
      "name": "Caninos",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 59,
      "name": "Arcanin",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 60,
      "name": "Ptitard",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 61,
      "name": "Têtarte",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 62,
      "name": "Tartard",
      "types": [
        "Eau",
        "Combat"
      ]
    },
    {
      "index": 63,
      "name": "Abra",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 64,
      "name": "Kadabra",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 65,
      "name": "Alakazam",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 66,
      "name": "Machoc",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 67,
      "name": "Machopeur",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 68,
      "name": "Mackogneur",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 69,
      "name": "Chétiflor",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 70,
      "name": "Boustiflor",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 71,
      "name": "Empiflor",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 72,
      "name": "Tentacool",
      "types": [
        "Eau",
        "Poison"
      ]
    },
    {
      "index": 73,
      "name": "Tentacruel",
      "types": [
        "Eau",
        "Poison"
      ]
    },
    {
      "index": 74,
      "name": "Racaillou",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 75,
      "name": "Gravalanch",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 76,
      "name": "Grolem",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 77,
      "name": "Ponyta",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 78,
      "name": "Galopa",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 79,
      "name": "Ramoloss",
      "types": [
        "Eau",
        "Psy"
      ]
    },
    {
      "index": 80,
      "name": "Flagadoss",
      "types": [
        "Eau",
        "Psy"
      ]
    },
    {
      "index": 81,
      "name": "Magnéti",
      "types": [
        "Électrik",
        "Acier"
      ]
    },
    {
      "index": 82,
      "name": "Magnéton",
      "types": [
        "Électrik",
        "Acier"
      ]
    },
    {
      "index": 83,
      "name": "Canarticho",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 84,
      "name": "Doduo",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 85,
      "name": "Dodrio",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 86,
      "name": "Otaria",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 87,
      "name": "Lamantine",
      "types": [
        "Eau",
        "Glace"
      ]
    },
    {
      "index": 88,
      "name": "Tadmorv",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 89,
      "name": "Grotadmorv",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 90,
      "name": "Kokiyas",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 91,
      "name": "Crustabri",
      "types": [
        "Eau",
        "Glace"
      ]
    },
    {
      "index": 92,
      "name": "Fantominus",
      "types": [
        "Spectre",
        "Poison"
      ]
    },
    {
      "index": 93,
      "name": "Spectrum",
      "types": [
        "Spectre",
        "Poison"
      ]
    },
    {
      "index": 94,
      "name": "Ectoplasma",
      "types": [
        "Spectre",
        "Poison"
      ]
    },
    {
      "index": 95,
      "name": "Onix",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 96,
      "name": "Soporifik",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 97,
      "name": "Hypnomade",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 98,
      "name": "Krabby",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 99,
      "name": "Krabboss",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 100,
      "name": "Voltorbe",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 101,
      "name": "Électrode",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 102,
      "name": "Noeunoeuf",
      "types": [
        "Plante",
        "Psy"
      ]
    },
    {
      "index": 103,
      "name": "Noadkoko",
      "types": [
        "Plante",
        "Psy"
      ]
    },
    {
      "index": 104,
      "name": "Osselait",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 105,
      "name": "Ossatueur",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 106,
      "name": "Kicklee",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 107,
      "name": "Tygnon",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 108,
      "name": "Excelangue",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 109,
      "name": "Smogo",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 110,
      "name": "Smogogo",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 111,
      "name": "Rhinocorne",
      "types": [
        "Sol",
        "Roche"
      ]
    },
    {
      "index": 112,
      "name": "Rhinoféros",
      "types": [
        "Sol",
        "Roche"
      ]
    },
    {
      "index": 113,
      "name": "Leveinard",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 114,
      "name": "Saquedeneu",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 115,
      "name": "Kangourex",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 116,
      "name": "Hypotrempe",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 117,
      "name": "Hypocéan",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 118,
      "name": "Poissirène",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 119,
      "name": "Poissoroy",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 120,
      "name": "Stari",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 121,
      "name": "Staross",
      "types": [
        "Eau",
        "Psy"
      ]
    },
    {
      "index": 122,
      "name": "M. Mime",
      "types": [
        "Psy",
        "Fée"
      ]
    },
    {
      "index": 123,
      "name": "Insécateur",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 124,
      "name": "Lippoutou",
      "types": [
        "Glace",
        "Psy"
      ]
    },
    {
      "index": 125,
      "name": "Élektek",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 126,
      "name": "Magmar",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 127,
      "name": "Scarabrute",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 128,
      "name": "Tauros",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 129,
      "name": "Magicarpe",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 130,
      "name": "Léviator",
      "types": [
        "Eau",
        "Vol"
      ]
    },
    {
      "index": 131,
      "name": "Lokhlass",
      "types": [
        "Eau",
        "Glace"
      ]
    },
    {
      "index": 132,
      "name": "Métamorph",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 133,
      "name": "Évoli",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 134,
      "name": "Aquali",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 135,
      "name": "Voltali",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 136,
      "name": "Pyroli",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 137,
      "name": "Porygon",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 138,
      "name": "Amonita",
      "types": [
        "Roche",
        "Eau"
      ]
    },
    {
      "index": 139,
      "name": "Amonistar",
      "types": [
        "Roche",
        "Eau"
      ]
    },
    {
      "index": 140,
      "name": "Kabuto",
      "types": [
        "Roche",
        "Eau"
      ]
    },
    {
      "index": 141,
      "name": "Kabutops",
      "types": [
        "Roche",
        "Eau"
      ]
    },
    {
      "index": 142,
      "name": "Ptéra",
      "types": [
        "Roche",
        "Vol"
      ]
    },
    {
      "index": 143,
      "name": "Ronflex",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 144,
      "name": "Artikodin",
      "types": [
        "Glace",
        "Vol"
      ]
    },
    {
      "index": 145,
      "name": "Électhor",
      "types": [
        "Électrik",
        "Vol"
      ]
    },
    {
      "index": 146,
      "name": "Sulfura",
      "types": [
        "Feu",
        "Vol"
      ]
    },
    {
      "index": 147,
      "name": "Minidraco",
      "types": [
        "Dragon",
        null
      ]
    },
    {
      "index": 148,
      "name": "Draco",
      "types": [
        "Dragon",
        null
      ]
    },
    {
      "index": 149,
      "name": "Dracolosse",
      "types": [
        "Dragon",
        "Vol"
      ]
    },
    {
      "index": 150,
      "name": "Mewtwo",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 151,
      "name": "Mew",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 152,
      "name": "Germignon",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 153,
      "name": "Macronium",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 154,
      "name": "Méganium",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 155,
      "name": "Héricendre",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 156,
      "name": "Feurisson",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 157,
      "name": "Typhlosion",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 158,
      "name": "Kaiminus",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 159,
      "name": "Crocrodil",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 160,
      "name": "Aligatueur",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 161,
      "name": "Fouinette",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 162,
      "name": "Fouinar",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 163,
      "name": "Hoothoot",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 164,
      "name": "Noarfang",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 165,
      "name": "Coxy",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 166,
      "name": "Coxyclaque",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 167,
      "name": "Mimigal",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 168,
      "name": "Migalos",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 169,
      "name": "Nostenfer",
      "types": [
        "Poison",
        "Vol"
      ]
    },
    {
      "index": 170,
      "name": "Loupio",
      "types": [
        "Eau",
        "Électrik"
      ]
    },
    {
      "index": 171,
      "name": "Lanturn",
      "types": [
        "Eau",
        "Électrik"
      ]
    },
    {
      "index": 172,
      "name": "Pichu",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 173,
      "name": "Mélo",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 174,
      "name": "Toudoudou",
      "types": [
        "Normal",
        "Fée"
      ]
    },
    {
      "index": 175,
      "name": "Togepi",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 176,
      "name": "Togetic",
      "types": [
        "Fée",
        "Vol"
      ]
    },
    {
      "index": 177,
      "name": "Natu",
      "types": [
        "Psy",
        "Vol"
      ]
    },
    {
      "index": 178,
      "name": "Xatu",
      "types": [
        "Psy",
        "Vol"
      ]
    },
    {
      "index": 179,
      "name": "Wattouat",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 180,
      "name": "Lainergie",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 181,
      "name": "Pharamp",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 182,
      "name": "Joliflor",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 183,
      "name": "Marill",
      "types": [
        "Eau",
        "Fée"
      ]
    },
    {
      "index": 184,
      "name": "Azumarill",
      "types": [
        "Eau",
        "Fée"
      ]
    },
    {
      "index": 185,
      "name": "Simularbre",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 186,
      "name": "Tarpaud",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 187,
      "name": "Granivol",
      "types": [
        "Plante",
        "Vol"
      ]
    },
    {
      "index": 188,
      "name": "Floravol",
      "types": [
        "Plante",
        "Vol"
      ]
    },
    {
      "index": 189,
      "name": "Cotovol",
      "types": [
        "Plante",
        "Vol"
      ]
    },
    {
      "index": 190,
      "name": "Capumain",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 191,
      "name": "Tournegrin",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 192,
      "name": "Héliatronc",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 193,
      "name": "Yanma",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 194,
      "name": "Axoloto",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 195,
      "name": "Maraiste",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 196,
      "name": "Mentali",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 197,
      "name": "Noctali",
      "types": [
        "Ténèbres",
        null
      ]
    },
    {
      "index": 198,
      "name": "Cornèbre",
      "types": [
        "Ténèbres",
        "Vol"
      ]
    },
    {
      "index": 199,
      "name": "Roigada",
      "types": [
        "Eau",
        "Psy"
      ]
    },
    {
      "index": 200,
      "name": "Feuforêve",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 201,
      "name": "Zarbi",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 202,
      "name": "Qulbutoké",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 203,
      "name": "Girafarig",
      "types": [
        "Normal",
        "Psy"
      ]
    },
    {
      "index": 204,
      "name": "Pomdepik",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 205,
      "name": "Foretress",
      "types": [
        "Insecte",
        "Acier"
      ]
    },
    {
      "index": 206,
      "name": "Insolourdo",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 207,
      "name": "Scorplane",
      "types": [
        "Sol",
        "Vol"
      ]
    },
    {
      "index": 208,
      "name": "Steelix",
      "types": [
        "Acier",
        "Sol"
      ]
    },
    {
      "index": 209,
      "name": "Snubbull",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 210,
      "name": "Granbull",
      "types": [
        "Fée",
        null
      ]
    },
    {
      "index": 211,
      "name": "Qwilfish",
      "types": [
        "Eau",
        "Poison"
      ]
    },
    {
      "index": 212,
      "name": "Cizayox",
      "types": [
        "Insecte",
        "Acier"
      ]
    },
    {
      "index": 213,
      "name": "Caratroc",
      "types": [
        "Insecte",
        "Roche"
      ]
    },
    {
      "index": 214,
      "name": "Scarhino",
      "types": [
        "Insecte",
        "Combat"
      ]
    },
    {
      "index": 215,
      "name": "Farfuret",
      "types": [
        "Ténèbres",
        "Glace"
      ]
    },
    {
      "index": 216,
      "name": "Teddiursa",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 217,
      "name": "Ursaring",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 218,
      "name": "Limagma",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 219,
      "name": "Volcaropod",
      "types": [
        "Feu",
        "Roche"
      ]
    },
    {
      "index": 220,
      "name": "Marcacrin",
      "types": [
        "Glace",
        "Sol"
      ]
    },
    {
      "index": 221,
      "name": "Cochignon",
      "types": [
        "Glace",
        "Sol"
      ]
    },
    {
      "index": 222,
      "name": "Corayon",
      "types": [
        "Eau",
        "Roche"
      ]
    },
    {
      "index": 223,
      "name": "Rémoraid",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 224,
      "name": "Octillery",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 225,
      "name": "Cadoizo",
      "types": [
        "Glace",
        "Vol"
      ]
    },
    {
      "index": 226,
      "name": "Démanta",
      "types": [
        "Eau",
        "Vol"
      ]
    },
    {
      "index": 227,
      "name": "Airmure",
      "types": [
        "Acier",
        "Vol"
      ]
    },
    {
      "index": 228,
      "name": "Malosse",
      "types": [
        "Ténèbres",
        "Feu"
      ]
    },
    {
      "index": 229,
      "name": "Démolosse",
      "types": [
        "Ténèbres",
        "Feu"
      ]
    },
    {
      "index": 230,
      "name": "Hyporoi",
      "types": [
        "Eau",
        "Dragon"
      ]
    },
    {
      "index": 231,
      "name": "Phanpy",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 232,
      "name": "Donphan",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 233,
      "name": "Porygon2",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 234,
      "name": "Cerfrousse",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 235,
      "name": "Queulorior",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 236,
      "name": "Debugant",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 237,
      "name": "Kapoera",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 238,
      "name": "Lippouti",
      "types": [
        "Glace",
        "Psy"
      ]
    },
    {
      "index": 239,
      "name": "Élekid",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 240,
      "name": "Magby",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 241,
      "name": "Écrémeuh",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 242,
      "name": "Leuphorie",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 243,
      "name": "Raikou",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 244,
      "name": "Entei",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 245,
      "name": "Suicune",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 246,
      "name": "Embrylex",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 247,
      "name": "Ymphect",
      "types": [
        "Roche",
        "Sol"
      ]
    },
    {
      "index": 248,
      "name": "Tyranocif",
      "types": [
        "Roche",
        "Ténèbres"
      ]
    },
    {
      "index": 249,
      "name": "Lugia",
      "types": [
        "Psy",
        "Vol"
      ]
    },
    {
      "index": 250,
      "name": "Ho-Oh",
      "types": [
        "Feu",
        "Vol"
      ]
    },
    {
      "index": 251,
      "name": "Celebi",
      "types": [
        "Psy",
        "Plante"
      ]
    },
    {
      "index": 252,
      "name": "Arcko",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 253,
      "name": "Massko",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 254,
      "name": "Jungko",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 255,
      "name": "Poussifeu",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 256,
      "name": "Galifeu",
      "types": [
        "Feu",
        "Combat"
      ]
    },
    {
      "index": 257,
      "name": "Braségali",
      "types": [
        "Feu",
        "Combat"
      ]
    },
    {
      "index": 258,
      "name": "Gobou",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 259,
      "name": "Flobio",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 260,
      "name": "Laggron",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 261,
      "name": "Medhyèna",
      "types": [
        "Ténèbres",
        null
      ]
    },
    {
      "index": 262,
      "name": "Grahyèna",
      "types": [
        "Ténèbres",
        null
      ]
    },
    {
      "index": 263,
      "name": "Zigzaton",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 264,
      "name": "Linéon",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 265,
      "name": "Chenipotte",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 266,
      "name": "Armulys",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 267,
      "name": "Charmillon",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 268,
      "name": "Blindalys",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 269,
      "name": "Papinox",
      "types": [
        "Insecte",
        "Poison"
      ]
    },
    {
      "index": 270,
      "name": "Nénupiot",
      "types": [
        "Eau",
        "Plante"
      ]
    },
    {
      "index": 271,
      "name": "Lombre",
      "types": [
        "Eau",
        "Plante"
      ]
    },
    {
      "index": 272,
      "name": "Ludicolo",
      "types": [
        "Eau",
        "Plante"
      ]
    },
    {
      "index": 273,
      "name": "Grainipiot",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 274,
      "name": "Pifeuil",
      "types": [
        "Plante",
        "Ténèbres"
      ]
    },
    {
      "index": 275,
      "name": "Tengalice",
      "types": [
        "Plante",
        "Ténèbres"
      ]
    },
    {
      "index": 276,
      "name": "Nirondelle",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 277,
      "name": "Hélédelle",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 278,
      "name": "Goélise",
      "types": [
        "Eau",
        "Vol"
      ]
    },
    {
      "index": 279,
      "name": "Bekipan",
      "types": [
        "Eau",
        "Vol"
      ]
    },
    {
      "index": 280,
      "name": "Tarsal",
      "types": [
        "Psy",
        "Fée"
      ]
    },
    {
      "index": 281,
      "name": "Kirlia",
      "types": [
        "Psy",
        "Fée"
      ]
    },
    {
      "index": 282,
      "name": "Gardevoir",
      "types": [
        "Psy",
        "Fée"
      ]
    },
    {
      "index": 283,
      "name": "Arakdo",
      "types": [
        "Insecte",
        "Eau"
      ]
    },
    {
      "index": 284,
      "name": "Maskadra",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 285,
      "name": "Balignon",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 286,
      "name": "Chapignon",
      "types": [
        "Plante",
        "Combat"
      ]
    },
    {
      "index": 287,
      "name": "Parecool",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 288,
      "name": "Vigoroth",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 289,
      "name": "Monaflèmit",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 290,
      "name": "Ningale",
      "types": [
        "Insecte",
        "Sol"
      ]
    },
    {
      "index": 291,
      "name": "Ninjask",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 292,
      "name": "Munja",
      "types": [
        "Insecte",
        "Spectre"
      ]
    },
    {
      "index": 293,
      "name": "Chuchmur",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 294,
      "name": "Ramboum",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 295,
      "name": "Brouhabam",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 296,
      "name": "Makuhita",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 297,
      "name": "Hariyama",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 298,
      "name": "Azurill",
      "types": [
        "Normal",
        "Fée"
      ]
    },
    {
      "index": 299,
      "name": "Tarinor",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 300,
      "name": "Skitty",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 301,
      "name": "Delcatty",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 302,
      "name": "Ténéfix",
      "types": [
        "Ténèbres",
        "Spectre"
      ]
    },
    {
      "index": 303,
      "name": "Mysdibule",
      "types": [
        "Acier",
        "Fée"
      ]
    },
    {
      "index": 304,
      "name": "Galekid",
      "types": [
        "Acier",
        "Roche"
      ]
    },
    {
      "index": 305,
      "name": "Galegon",
      "types": [
        "Acier",
        "Roche"
      ]
    },
    {
      "index": 306,
      "name": "Galeking",
      "types": [
        "Acier",
        "Roche"
      ]
    },
    {
      "index": 307,
      "name": "Méditikka",
      "types": [
        "Combat",
        "Psy"
      ]
    },
    {
      "index": 308,
      "name": "Charmina",
      "types": [
        "Combat",
        "Psy"
      ]
    },
    {
      "index": 309,
      "name": "Dynavolt",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 310,
      "name": "Élecsprint",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 311,
      "name": "Posipi",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 312,
      "name": "Négapi",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 313,
      "name": "Muciole",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 314,
      "name": "Lumivole",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 315,
      "name": "Rosélia",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 316,
      "name": "Gloupti",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 317,
      "name": "Avaltout",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 318,
      "name": "Carvanha",
      "types": [
        "Eau",
        "Ténèbres"
      ]
    },
    {
      "index": 319,
      "name": "Sharpedo",
      "types": [
        "Eau",
        "Ténèbres"
      ]
    },
    {
      "index": 320,
      "name": "Wailmer",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 321,
      "name": "Wailord",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 322,
      "name": "Chamallot",
      "types": [
        "Feu",
        "Sol"
      ]
    },
    {
      "index": 323,
      "name": "Camérupt",
      "types": [
        "Feu",
        "Sol"
      ]
    },
    {
      "index": 324,
      "name": "Chartor",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 325,
      "name": "Spoink",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 326,
      "name": "Groret",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 327,
      "name": "Spinda",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 328,
      "name": "Kraknoix",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 329,
      "name": "Vibraninf",
      "types": [
        "Sol",
        "Dragon"
      ]
    },
    {
      "index": 330,
      "name": "Libégon",
      "types": [
        "Sol",
        "Dragon"
      ]
    },
    {
      "index": 331,
      "name": "Cacnea",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 332,
      "name": "Cacturne",
      "types": [
        "Plante",
        "Ténèbres"
      ]
    },
    {
      "index": 333,
      "name": "Tylton",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 334,
      "name": "Altaria",
      "types": [
        "Dragon",
        "Vol"
      ]
    },
    {
      "index": 335,
      "name": "Mangriff",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 336,
      "name": "Séviper",
      "types": [
        "Poison",
        null
      ]
    },
    {
      "index": 337,
      "name": "Séléroc",
      "types": [
        "Roche",
        "Psy"
      ]
    },
    {
      "index": 338,
      "name": "Solaroc",
      "types": [
        "Roche",
        "Psy"
      ]
    },
    {
      "index": 339,
      "name": "Barloche",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 340,
      "name": "Barbicha",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 341,
      "name": "Écrapince",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 342,
      "name": "Colhomard",
      "types": [
        "Eau",
        "Ténèbres"
      ]
    },
    {
      "index": 343,
      "name": "Balbuto",
      "types": [
        "Sol",
        "Psy"
      ]
    },
    {
      "index": 344,
      "name": "Kaorine",
      "types": [
        "Sol",
        "Psy"
      ]
    },
    {
      "index": 345,
      "name": "Lilia",
      "types": [
        "Roche",
        "Plante"
      ]
    },
    {
      "index": 346,
      "name": "Vacilys",
      "types": [
        "Roche",
        "Plante"
      ]
    },
    {
      "index": 347,
      "name": "Anorith",
      "types": [
        "Roche",
        "Insecte"
      ]
    },
    {
      "index": 348,
      "name": "Armaldo",
      "types": [
        "Roche",
        "Insecte"
      ]
    },
    {
      "index": 349,
      "name": "Barpau",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 350,
      "name": "Milobellus",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 351,
      "name": "Morphéo",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 352,
      "name": "Kecleon",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 353,
      "name": "Polichombr",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 354,
      "name": "Branette",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 355,
      "name": "Skelénox",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 356,
      "name": "Téraclope",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 357,
      "name": "Tropius",
      "types": [
        "Plante",
        "Vol"
      ]
    },
    {
      "index": 358,
      "name": "Éoko",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 359,
      "name": "Absol",
      "types": [
        "Ténèbres",
        null
      ]
    },
    {
      "index": 360,
      "name": "Okéoké",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 361,
      "name": "Stalgamin",
      "types": [
        "Glace",
        null
      ]
    },
    {
      "index": 362,
      "name": "Oniglali",
      "types": [
        "Glace",
        null
      ]
    },
    {
      "index": 363,
      "name": "Obalie",
      "types": [
        "Glace",
        "Eau"
      ]
    },
    {
      "index": 364,
      "name": "Phogleur",
      "types": [
        "Glace",
        "Eau"
      ]
    },
    {
      "index": 365,
      "name": "Kaimorse",
      "types": [
        "Glace",
        "Eau"
      ]
    },
    {
      "index": 366,
      "name": "Coquiperl",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 367,
      "name": "Serpang",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 368,
      "name": "Rosabyss",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 369,
      "name": "Relicanth",
      "types": [
        "Eau",
        "Roche"
      ]
    },
    {
      "index": 370,
      "name": "Lovdisc",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 371,
      "name": "Draby",
      "types": [
        "Dragon",
        null
      ]
    },
    {
      "index": 372,
      "name": "Drackhaus",
      "types": [
        "Dragon",
        null
      ]
    },
    {
      "index": 373,
      "name": "Drattak",
      "types": [
        "Dragon",
        "Vol"
      ]
    },
    {
      "index": 374,
      "name": "Terhal",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 375,
      "name": "Métang",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 376,
      "name": "Métalosse",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 377,
      "name": "Regirock",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 378,
      "name": "Regice",
      "types": [
        "Glace",
        null
      ]
    },
    {
      "index": 379,
      "name": "Registeel",
      "types": [
        "Acier",
        null
      ]
    },
    {
      "index": 380,
      "name": "Latias",
      "types": [
        "Dragon",
        "Psy"
      ]
    },
    {
      "index": 381,
      "name": "Latios",
      "types": [
        "Dragon",
        "Psy"
      ]
    },
    {
      "index": 382,
      "name": "Kyogre",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 383,
      "name": "Groudon",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 384,
      "name": "Rayquaza",
      "types": [
        "Dragon",
        "Vol"
      ]
    },
    {
      "index": 385,
      "name": "Jirachi",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 386,
      "name": "Deoxys",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 387,
      "name": "Tortipouss",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 388,
      "name": "Boskara",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 389,
      "name": "Torterra",
      "types": [
        "Plante",
        "Sol"
      ]
    },
    {
      "index": 390,
      "name": "Ouisticram",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 391,
      "name": "Chimpenfeu",
      "types": [
        "Feu",
        "Combat"
      ]
    },
    {
      "index": 392,
      "name": "Simiabraz",
      "types": [
        "Feu",
        "Combat"
      ]
    },
    {
      "index": 393,
      "name": "Tiplouf",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 394,
      "name": "Prinplouf",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 395,
      "name": "Pingoléon",
      "types": [
        "Eau",
        "Acier"
      ]
    },
    {
      "index": 396,
      "name": "Étourmi",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 397,
      "name": "Étourvol",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 398,
      "name": "Étouraptor",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 399,
      "name": "Keunotor",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 400,
      "name": "Castorno",
      "types": [
        "Normal",
        "Eau"
      ]
    },
    {
      "index": 401,
      "name": "Crikzik",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 402,
      "name": "Mélokrik",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 403,
      "name": "Lixy",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 404,
      "name": "Luxio",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 405,
      "name": "Luxray",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 406,
      "name": "Rozbouton",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 407,
      "name": "Roserade",
      "types": [
        "Plante",
        "Poison"
      ]
    },
    {
      "index": 408,
      "name": "Kranidos",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 409,
      "name": "Charkos",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 410,
      "name": "Dinoclier",
      "types": [
        "Roche",
        "Acier"
      ]
    },
    {
      "index": 411,
      "name": "Bastiodon",
      "types": [
        "Roche",
        "Acier"
      ]
    },
    {
      "index": 412,
      "name": "Cheniti",
      "types": [
        "Insecte",
        null
      ]
    },
    {
      "index": 413,
      "name": "Cheniselle",
      "types": [
        "Insecte",
        "Plante"
      ]
    },
    {
      "index": 414,
      "name": "Papilord",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 415,
      "name": "Apitrini",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 416,
      "name": "Apireine",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 417,
      "name": "Pachirisu",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 418,
      "name": "Mustébouée",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 419,
      "name": "Mustéflott",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 420,
      "name": "Ceribou",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 421,
      "name": "Ceriflor",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 422,
      "name": "Sancoki",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 423,
      "name": "Tritosor",
      "types": [
        "Eau",
        "Sol"
      ]
    },
    {
      "index": 424,
      "name": "Capidextre",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 425,
      "name": "Baudrive",
      "types": [
        "Spectre",
        "Vol"
      ]
    },
    {
      "index": 426,
      "name": "Grodrive",
      "types": [
        "Spectre",
        "Vol"
      ]
    },
    {
      "index": 427,
      "name": "Laporeille",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 428,
      "name": "Lockpin",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 429,
      "name": "Magirêve",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 430,
      "name": "Corboss",
      "types": [
        "Ténèbres",
        "Vol"
      ]
    },
    {
      "index": 431,
      "name": "Chaglam",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 432,
      "name": "Chaffreux",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 433,
      "name": "Korillon",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 434,
      "name": "Moufouette",
      "types": [
        "Poison",
        "Ténèbres"
      ]
    },
    {
      "index": 435,
      "name": "Moufflair",
      "types": [
        "Poison",
        "Ténèbres"
      ]
    },
    {
      "index": 436,
      "name": "Archéomire",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 437,
      "name": "Archéodong",
      "types": [
        "Acier",
        "Psy"
      ]
    },
    {
      "index": 438,
      "name": "Manzaï",
      "types": [
        "Roche",
        null
      ]
    },
    {
      "index": 439,
      "name": "Mime Jr.",
      "types": [
        "Psy",
        "Fée"
      ]
    },
    {
      "index": 440,
      "name": "Ptiravi",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 441,
      "name": "Pijako",
      "types": [
        "Normal",
        "Vol"
      ]
    },
    {
      "index": 442,
      "name": "Spiritomb",
      "types": [
        "Spectre",
        "Ténèbres"
      ]
    },
    {
      "index": 443,
      "name": "Griknot",
      "types": [
        "Dragon",
        "Sol"
      ]
    },
    {
      "index": 444,
      "name": "Carmache",
      "types": [
        "Dragon",
        "Sol"
      ]
    },
    {
      "index": 445,
      "name": "Carchacrok",
      "types": [
        "Dragon",
        "Sol"
      ]
    },
    {
      "index": 446,
      "name": "Goinfrex",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 447,
      "name": "Riolu",
      "types": [
        "Combat",
        null
      ]
    },
    {
      "index": 448,
      "name": "Lucario",
      "types": [
        "Combat",
        "Acier"
      ]
    },
    {
      "index": 449,
      "name": "Hippopotas",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 450,
      "name": "Hippodocus",
      "types": [
        "Sol",
        null
      ]
    },
    {
      "index": 451,
      "name": "Rapion",
      "types": [
        "Poison",
        "Insecte"
      ]
    },
    {
      "index": 452,
      "name": "Drascore",
      "types": [
        "Poison",
        "Ténèbres"
      ]
    },
    {
      "index": 453,
      "name": "Cradopaud",
      "types": [
        "Poison",
        "Combat"
      ]
    },
    {
      "index": 454,
      "name": "Coatox",
      "types": [
        "Poison",
        "Combat"
      ]
    },
    {
      "index": 455,
      "name": "Vortente",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 456,
      "name": "Écayon",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 457,
      "name": "Luminéon",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 458,
      "name": "Babimanta",
      "types": [
        "Eau",
        "Vol"
      ]
    },
    {
      "index": 459,
      "name": "Blizzi",
      "types": [
        "Plante",
        "Glace"
      ]
    },
    {
      "index": 460,
      "name": "Blizzaroi",
      "types": [
        "Plante",
        "Glace"
      ]
    },
    {
      "index": 461,
      "name": "Dimoret",
      "types": [
        "Ténèbres",
        "Glace"
      ]
    },
    {
      "index": 462,
      "name": "Magnézone",
      "types": [
        "Électrik",
        "Acier"
      ]
    },
    {
      "index": 463,
      "name": "Coudlangue",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 464,
      "name": "Rhinastoc",
      "types": [
        "Sol",
        "Roche"
      ]
    },
    {
      "index": 465,
      "name": "Bouldeneu",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 466,
      "name": "Élekable",
      "types": [
        "Électrik",
        null
      ]
    },
    {
      "index": 467,
      "name": "Maganon",
      "types": [
        "Feu",
        null
      ]
    },
    {
      "index": 468,
      "name": "Togekiss",
      "types": [
        "Fée",
        "Vol"
      ]
    },
    {
      "index": 469,
      "name": "Yanmega",
      "types": [
        "Insecte",
        "Vol"
      ]
    },
    {
      "index": 470,
      "name": "Phyllali",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 471,
      "name": "Givrali",
      "types": [
        "Glace",
        null
      ]
    },
    {
      "index": 472,
      "name": "Scorvol",
      "types": [
        "Sol",
        "Vol"
      ]
    },
    {
      "index": 473,
      "name": "Mammochon",
      "types": [
        "Glace",
        "Sol"
      ]
    },
    {
      "index": 474,
      "name": "Porygon-Z",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 475,
      "name": "Gallame",
      "types": [
        "Psy",
        "Combat"
      ]
    },
    {
      "index": 476,
      "name": "Tarinorme",
      "types": [
        "Roche",
        "Acier"
      ]
    },
    {
      "index": 477,
      "name": "Noctunoir",
      "types": [
        "Spectre",
        null
      ]
    },
    {
      "index": 478,
      "name": "Momartik",
      "types": [
        "Glace",
        "Spectre"
      ]
    },
    {
      "index": 479,
      "name": "Motisma",
      "types": [
        "Électrik",
        "Spectre"
      ]
    },
    {
      "index": 480,
      "name": "Créhelf",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 481,
      "name": "Créfollet",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 482,
      "name": "Créfadet",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 483,
      "name": "Dialga",
      "types": [
        "Acier",
        "Dragon"
      ]
    },
    {
      "index": 484,
      "name": "Palkia",
      "types": [
        "Eau",
        "Dragon"
      ]
    },
    {
      "index": 485,
      "name": "Heatran",
      "types": [
        "Feu",
        "Acier"
      ]
    },
    {
      "index": 486,
      "name": "Regigigas",
      "types": [
        "Normal",
        null
      ]
    },
    {
      "index": 487,
      "name": "Giratina",
      "types": [
        "Spectre",
        "Dragon"
      ]
    },
    {
      "index": 488,
      "name": "Cresselia",
      "types": [
        "Psy",
        null
      ]
    },
    {
      "index": 489,
      "name": "Phione",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 490,
      "name": "Manaphy",
      "types": [
        "Eau",
        null
      ]
    },
    {
      "index": 491,
      "name": "Darkrai",
      "types": [
        "Ténèbres",
        null
      ]
    },
    {
      "index": 492,
      "name": "Shaymin",
      "types": [
        "Plante",
        null
      ]
    },
    {
      "index": 493,
      "name": "Arceus",
      "types": [
        "Normal",
        null
      ]
    }
  ]