import { data } from './pokedex.data';

function generation(index : number) {
    if (index <= 151) return 1;
    if (index <= 251) return 2;
    if (index <= 386) return 3;
    return 4;
}

export { data, generation };