import { readFileSync } from 'fs';
import * as Discord from 'discord.js';
import { Command } from './Command.js';
import * as Skribbl from './skribbl.io.js';
import * as Pokedex from './Pokedex';
import * as Zelda from './Zelda';
import * as Mario from './Mario';
import * as Lol from './Lol';
import * as SmashBros from './SmashBros';

const client = new Discord.Client();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (msg) => {

    if (msg.channel instanceof Discord.TextChannel) {
        if ((msg.channel.name === 'bot-test' || msg.channel.name === 'skribbl-io') && msg.content.startsWith('!skribbl.io')) {

            const command = Command.parse(msg.content);
            console.log(command);

            let wordsList : Array<String> = [];
            let replyMessage : Discord.Message;

            let game : Skribbl.Game = new Skribbl.Game({
                delayBeforeGameStarts: command.args['--await'] ? Skribbl.AWAIT : 15000,
                drawTime: 90,
                roundNumber: 5
            });

            game.onStart = () => {
                if (replyMessage)
                    replyMessage.delete().catch(reason => console.log(reason));

                if (msg.channel instanceof Discord.TextChannel)
                    if (msg.channel.name === 'skribbl-io')
                        msg.delete().catch(reason => console.log(reason));
            };

            game.onConnect = () => {
                msg.reply(
                    'Join with <' + game.url + '>.\n'
                    + (command.args['--await']  ? 'React to this message when you are ready to start the game.'
                                                : 'This message will expire in 15 seconds.'))
                    .then(createdMsg => {
                        replyMessage = createdMsg;
                        if (command.args['--await']) {
                            replyMessage.react('👌')
                                .then(() => {
                                    replyMessage.awaitReactions((reaction, user) => user.id === msg.author.id, { max: 1 })
                                        .then(() => game.start());
                                });
                        }
                    });
            };

            if (command.args['--pokemon']) {
                let pokemons = Pokedex.data;

                try {
                    if (command.args['--gen']) {
                        if (command.args['--gen'] === true)
                            throw new Error('Wrong usage');

                        let gens: Array<any> = command.args['--gen'].split(' ');
                        gens = gens.map((e) => parseInt(e));
                        pokemons = pokemons.filter((e) => gens.includes(Pokedex.generation(e.index)));
    
                        if (pokemons.length == 0)
                            throw new Error('Not enough words.');
    
                    }
                    wordsList = wordsList.concat(pokemons.map((e) => e.name));
                } catch(e) {
                    msg.reply('Usage:' + Skribbl.usage);
                }

            }
            if (command.args['--zelda']) {
                wordsList = wordsList.concat(Zelda.data);
            }
            if (command.args['--mario']) {
                wordsList = wordsList.concat(Mario.data);
            }
            if (command.args['--lol']) {
                wordsList = wordsList.concat(Lol.data);
            }
            if (command.args['--smash']) {
                wordsList = wordsList.concat(SmashBros.data);
            }

            if (wordsList.length != 0) {
                game.useCustomWords(wordsList.join(','));
                game.create();
            } else {
                console.log('Nothing happened.');
            }
        }
    }
});

const token = readFileSync('.token').toString().trim();
client.login(token);
